<?php
session_start();
include 'header.php';
?>
    <link rel="icon" href="../image/swiftgeek32.png" sizes="32x32" />
    <link rel="icon" href="../image/swiftgeek192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="../imageswift180apple.png" />
    <link rel="stylesheet" href="../css/loading.css" type="text/css">
    <link rel="stylesheet" href="../css/swift.css" type="text/css">
    <link href="../swiftbootcss/boot.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <title>Swift Geek Links - Frequetly Asked Questions</title>
  </head>

  <body>

    <div style="position:absolute; display: flex;width: 100%; height: 100%; justify-content: center; align-items: center;">
    <h4> Kindly Contact <a style="color: coral;" href="mailto:info@swiftgeek.com.ng">Info@swiftgeek.com.ng</a> should you have any question as regards our services, privacy. Thanks</h4>
    </div>

<div style="position: absolute; bottom: 0; left: 0; right: 0;">
<?php
include 'footer.php';
?>
</div>